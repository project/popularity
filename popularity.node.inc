<?php

function popularity_node_modifier_record($object, $value) {
  return array(
    'keys' => array('pid', 'type'),
    'data' => array(
      'pid' => $object->nid,
      'type' => 'node',
      'value' => $value,
    ),
  );
}

function popularity_node_modifier_list() {
  return array(
    'promote' => array(
      'title' => t('Content promotion to the front page'),
    ),
    'comment' => array(
      'title' => t('Comments'),
      'access' => module_exists('comment'),
    ),
    'statistics' => array(
      'title' => t('Statistics'),
      'access' => module_exists('statistics'),
    ),
    'trackback' => array(
      'title' => t('Trackback'),
      'access' => module_exists('trackback'),
    ),
  );
}

function popularity_node_modifier_values($name, $object) {
  $max = 1;

  switch ($name) {
    case 'promote':
      $value = $object->promote;
      break;
    case 'comment':
      $max = db_result(db_query("SELECT MAX(comment_count) FROM {node_comment_statistics}"));
      $value = $node->comment_count;
      break;
    case 'statistics':
      $max = db_result(db_query("SELECT MAX(totalcount) FROM {node_counter}"));
      $value = db_result(db_query("SELECT totalcount FROM {node_counter} WHERE nid = %d", array(':nid' => $object->nid)));
      break;
    case 'trackback':
      $max = db_result(db_query("SELECT MAX(COUNT(nid)) FROM {trackback_received} GROUP BY nid"));
      $value = db_result(db_query("SELECT COUNT(nid) FROM {trackback_received} WHERE nid = %d", array(':nid' => $node->nid)));
    default:
      $value = 0;
  }

  return (float) $value / (float) $max;
}
