
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Frequently Asked Questions (FAQ)
 * Known Issues
 * How Can You Contribute?


INTRODUCTION
------------

Current Maintainer: Dave Reid <http://drupal.org/user/53892>
Project Page: http://drupal.org/project/popularity

This is a Drupal implementation of Alex King's wonderful Popularity Contest
WordPress plugin (http://wordpress.org/extend/plugins/popularity-contest/).


INSTALLATION
------------

See http://drupal.org/getting-started/5/install-contrib for instructions on
how to install or update Drupal modules.

Once the popularity module is installed and enabled, you can configure
popularity ranking settings at admin/settings/popularity.


FREQUENTLY ASKED QUESTIONS
--------------------------

There are no frequently asked questions at this time.


KNOWN ISSUES
------------

There are no known issues at this time.

To report new bug reports, feature requests, and support requests, visit
http://drupal.org/project/issues/popularity.


HOW CAN YOU CONTRIBUTE?
---------------------

- Write a review for this module at drupalmodules.com.
  http://drupalmodules.com/module/popularity

- Help translate this module on launchpad.net.
  https://translations.launchpad.net/drupal-popularity

- Donate to the maintainer's replacement laptop fund to help keep development
  active. http://blog.davereid.net/content/laptop-fund

- Report any bugs, feature requests, etc. in the issue tracker.
  http://drupal.org/project/issues/popularity

- Contact the maintainer with any comments, questions, or feedback.
  http://davereid.net/contact
