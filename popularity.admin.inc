<?php

/**
 * @file
 * Admin page callbacks for the popularity module.
 */

function popularity_settings_form($form_state, $type) {
  module_load_include('inc', 'popularity', 'popularity.'. $type);

  $form['score'] = array(
    '#type' => 'fieldset',
    '#name' => t('Score modifiers'),
  );
  $modifiers = 'popularity_'. $type .'_modifier_list';
  foreach ($modifiers() as $name => $details) {
    $form['score']['popularity_'. $type .'_'. $name] = array(
      '#type' => 'select',
      '#title' => $details['title'],
      '#options' => array('0' => t('Exclude')) + drupal_map_assoc(range(1, 10)),
      '#default_value' => popularity_var($type .'_'. $name),
      '#access' => !isset($details['access']) || $details['access'],
    );
  }

  /*if (module_exists('jquery_ui')) {
    jquery_ui_add('ui.slider');
    drupal_add_js(drupal_get_path('module', 'popularity') .'/accessibleUISlider.jQuery.js');
    foreach ($form['score'] as $name => $element) {
      if (is_array($element) && $element['#type'] == 'select') {
        $id = 'edit-'. str_replace(array('][', '_', ' '), '-', $name);
        drupal_add_js('$(function(){ $(\'select#'. $id .'\').accessibleUISlider(); });', 'inline');
        //$form['score'][$name]['#prefix'] = '<noscript>';
        //$form['score'][$name]['#suffix'] = '</noscript>';
        //$form['score'][$name]['#suffix'] = '<script type="text/javascript"> $(\'select#'. $id .'\').accessibleUISlider(); </script>';
      }
    }
  }*/

  return system_settings_form($form);
}
